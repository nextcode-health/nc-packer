#!/bin/bash
# This script removes subscription in Redhat.
# Comment out the following two lines if your building a GA image

/usr/sbin/subscription-manager remove --all
/usr/sbin/subscription-manager unregister
/usr/sbin/subscription-manager clean

