#!/bin/bash
# Add VirtualBox additions to box

cd /tmp
mount -o loop /home/vagrant/VBoxGuestAdditions.iso /mnt
/mnt/VBoxLinuxAdditions.run --nox11
systemctl enable vboxadd-service.service
systemctl enable vboxadd.service
systemctl start vboxadd-service.service
systemctl start vboxadd.service
umount /mnt
/bin/rm -f /home/vagrant/VBoxGuestAdditions.iso
