#!/bin/bash

cd /etc
> ./resolv.conf

cd /tmp
#shred -uv *
rm -rf *
#shred -uv .*
rm -rf .*

cd /var/lib/systemd
#shred -uv ./random-seed
rm -rf ./random-seed

cd /var/log
> wtmp
> cron
> dmesg
> dmesg.old
> lastlog
> secure
> messages
> maillog

# stop network and remove leases
systemctl stop NetworkManager
killall dhclient

echo "Removing old leases files"
rm -Rf /var/lib/NetworkManager/*

# Cleanup log files
find /var/log -type f | while read f; do echo -ne '' > $f; done;

# Cleanup ssh host keys
rm -f /etc/ssh/ssh_host_*key*

rm -f /etc/udev/rules.d/70-persistent-net.rules
sed -i '/^UUID/d'   /etc/sysconfig/network-scripts/ifcfg-e*
sed -i '/^HWADDR/d' /etc/sysconfig/network-scripts/ifcfg-e*

cd /root
#shred -uv .lesshst .bash_history
rm -rf .lesshst .bash_history
