To run packer you obviously must have installed packer first. 
The rhel.json script makes 2 assumptions while running. 
1. That you have access to the rhel-server-7.4-x86_64-dvd.iso and that it is located in the same directory as the script
2. That you have virtualbox installed on the machine running the script
If you do not have the rhel-server iso, contact ito, Gummi or Hjörtur and they will be able to provide it.

To create a new rhel base image for vagrant, simply run packer build rhel.json.
By default we revoke the license back from Redhat in scripts/revoke.sh in the end.
This is so that we don't take up licenses when doing testing of this image. When
ready to build GA version you must comment out the lines in rhel-7.4/scripts/revoke.sh

Here is a list of a few things that can make your troubleshooting life easier if you run into them.

- If the run ends with a message stating that guest additions did not install, you will have problems with the box. Funky
   network issues along with not being able to mount properly. The reason for this is most likely updates to the kernel 
   while building without the necessary kernel packages installed. MAke sure no changes have been made to the rhel.json 
   file after you pulled it.
- If you use the box at all and intend to give it to someone else, you might run into issues. If you for example import 
   the box to your local vagrant and do port forwarding, those ports will be bound for whomever that will use the box as well.
- When doing testing with boxes, make sure you are not using the cached version of an old box. Look at the boxes 
   currently imported locally in ~/.vagrant.d/boxes. If you want to be absolutely sure you are running on your new box,
   remove the old one from ~/.vagrant.d/boxes and do a vagrant up.
