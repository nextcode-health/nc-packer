#!/bin/bash
user=`/bin/whoami`
llog=`/bin/lastlog -u $user`
cpu=`/bin/cat /proc/cpuinfo | /bin/grep 'model name' | /bin/head -1 | /bin/cut -d':' -f2`
memory=`/bin/free -m | /bin/head -n 2 | /bin/tail -n 1 | /bin/awk {'print $2'}`
White="\033[01;37m"
NoColor="\033[0m"
Blue="\033[01;34m"
Green="\033[0;32m"
IPV4="$(/usr/sbin/ip address show dev enp0s8 | /usr/bin/grep "inet " | /usr/bin/awk '{print $2}')"

/bin/echo -e "$Green===============================================================================$Blue
$White$llog$Blue
Hostname: $White$(/bin/hostname)$Blue.
Private IP Address: $White$IPV4$Blue
OS: $White$(/bin/cat /etc/system-release)$Blue
Kernel version: $White$(/bin/uname -r)$Blue
CPU type: $White$cpu$Blue
Memory size: $White$memory$Blue
$Green===============================================================================$NoColor"
/bin/echo
