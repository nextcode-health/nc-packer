NextCODE Packer Templates
=========================

# Requirements

You need

* VirtualBox
* vagrant
* qemu
* packer

# Running

To run packer on the templates you simply run packer build.

For example, to create a QStack image (qcow2):

    packer build --only=qemu packer-centos-7.json

or to create a virtualbox image:

    packer build --only=virtualbox packer-centos-7.json

## Caveats

### Disk size

You can control the disk size of the produced template by adjusting it inside each packer config file.

### Parallel builds

Packer supports (and defaults to) running parallel builds. However, you cannot run qemu and virtualbox both at the same time on one computer. Therefore you need to disable parallel builds (using the `-parallel=false` option) or just run one build at a time like in the examples above (using the `--only=builder` option).

### QEMU artifact

Due to a limitation of packer, it is impossible to get a pure qcow2 image file out of the qemu build. Instead we use the `compress` post-processor to create a tarball of the image. However, when creating the image, we fill up the disk of the VM by writing from /dev/zero to a file. This is done to overwrite any deleted files and enable compression of the VM disk image. This means that the qcow2 image inside the tarball can be quite large (the full size of the disk of the VM being created). Since packer does not support qemu-img as a post-processor, we need to extract the qcow image from the tarball and run it through the following command manually:

    qemu-img convert -O qcow2 large_vm_img.qcow2 vm_img.compressed.qcow2

This drawback has been reported on the packer GitHub page: https://github.com/mitchellh/packer/issues/644

# Quick testing of template

To see if the template boots and does most of what you want to do, you can run it on your packer server to do a quick glance through VNC, before registering and testing in QStack

	qemu-system-x86_64 -device virtio-net -drive file=debian-8.1-10240.qcow2,if=virtio,cache=writeback,discard=ignore -boot once=d -name test -machine type=pc,accel=kvm -m 4096M


# Registering templates in QStack

You can register them via the admin panel as described in the documentation, or via cloudmonkey

	# find ostypes
	cloudmonkey list ostypes

	cloudmonkey register template name="NextCODE CentOS 7 - 250GB" displaytext="NextCODE CentOS 7 - 250GB" zoneid=be25c4af-5c73-437c-9506-4cccc9aa5d52 hypervisor="kvm" format="qcow2" ostypeid=c329b220-d6cd-11e4-bd27-001a4a765a88 bits=64 url="http://cdn.nextcode.com/contrib/vm-images/nc-centos-7_x64-256000.compressed.qcow2" ispublic=true isfeatured=true

# License & Authors

This README and parts of the packer templates have been adapted from the GreenQloud packer repository at: https://github.com/greenqloud/packer/

- Adaptation to NextCODE environment: Stefan Freyr Stefansson (<stefan@wuxinextcode.com>)

- Author: Jon Thor Kristinsson (<jon@greenqloud.com>)
- Author: Jordi Amorós (<jordi@greenqloud.com>)
- Author: Kristinn Soffanías Rúnarsson (<soffi@greenqloud.com>)

```text
Copyright 2015, Greenqloud (<support@greenqloud.com>)
```